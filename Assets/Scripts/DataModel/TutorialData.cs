﻿using UnityEngine;
using SimpleJSON;
using System.Collections.Generic;

public class TutorialData
{
    public string Id { get; private set; }
    public TutorialStep[] Steps { get; private set; }
    public string Next { get; private set; }
    public List<string> Requirements { get; private set; }

    public TutorialData(JSONNode json)
    {
        Id = json["id"];
        Next = json["next"];

        Steps = new TutorialStep[json["step"].Count];
        for (int i = 0; i < Steps.Length; i++)
        {
            Steps[i] = new TutorialStep(json["step"][i]);
        }

        Requirements = new List<string>();
        for (int i = 0; i < json["req"].Count; i++)
        {
            Requirements.Add(json["req"][i]);
        }
    }
}

public class TutorialStep
{
    public TutorialType Type { get; private set; }
    public string Text { get; private set; }
    public TutorialTextPosition TextPosition { get; private set; }
    public string ObjectId { get; private set; }
    public string ObjectIdEnd { get; private set; }

    public bool HasText { get { return !string.IsNullOrEmpty(Text); } }

    public TutorialStep(JSONNode json)
    {
        Type = (TutorialType)json["type"].AsInt;

        if(Type == TutorialType.Monolog)
        {
            Text = json["text"].Value;
            ObjectId = string.Empty;
        }
        else if(Type == TutorialType.HandPointerClick)
        {
            Text = string.IsNullOrEmpty(json["text"].Value) ? string.Empty : json["text"].Value;
            ObjectId = json["target"].Value;
        }
        else if (Type == TutorialType.HandPointerDrag)
        {
            Text = string.IsNullOrEmpty(json["text"].Value) ? string.Empty : json["text"].Value;
            ObjectId = json["target"].Value;
            ObjectIdEnd = json["target_end"].Value;
        }
        else if (Type == TutorialType.Highlight)
        {
            Text = string.IsNullOrEmpty(json["text"].Value) ? string.Empty : json["text"].Value;
            ObjectId = json["target"].Value;
        }

        if (HasText)
        {
            if (!string.IsNullOrEmpty(json["position"]))
            {
                TextPosition = (TutorialTextPosition)System.Enum.Parse(typeof(TutorialTextPosition), json["position"]);
            }
            else
            {
                TextPosition = TutorialTextPosition.Top;
            }
        }
    }
}

public enum TutorialType
{
    Monolog = 0 , HandPointerClick = 1, HandPointerDrag = 2, Highlight = 3
}

public enum TutorialTextPosition
{
    Top, Bottom
}