﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneSplashManager : GameSceneManager
{
    [Header("Setting")]
    [SerializeField] SceneField nextScene;
    [SerializeField] SceneField loaderScene;
    [SerializeField] Slider sliderProgress;

    protected override void OnAwake()
    {

    }

    private IEnumerator Start()
    {
        AsyncOperation async = SceneController.LoadSceneAsync(loaderScene.SceneName, UnityEngine.SceneManagement.LoadSceneMode.Additive);

        while (!async.isDone)
        {
            if (sliderProgress != null)
            {
                sliderProgress.value = 0;
                sliderProgress.minValue = 0;
                sliderProgress.maxValue = 100;
                sliderProgress.value = Mathf.Floor(async.progress * 100f);
            }

            yield return new WaitForEndOfFrame();
        }

        SceneController.UnloadSceneAsync(loaderScene.SceneName);
        AppManager.Instance.InitApplication(() =>
        {
            if (sliderProgress != null)
            {
                sliderProgress.value = 100f;
            }

            EndSplash();
        });
    }

    private void EndSplash()
    {
        StartCoroutine(_EndSplash());
    }

    private IEnumerator _EndSplash()
    {
#if !UNITY_EDITOR
        yield return new WaitForSecondsRealtime(2f);
#else
        yield return new WaitForEndOfFrame();
#endif
        StartTransition(nextScene.SceneName);
    }
}