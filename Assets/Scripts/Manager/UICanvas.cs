﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICanvas : MonoBehaviour
{
    protected System.Action<GameObject> OnCanceled;

    private void Awake()
    {
        OnAwake();
    }

    private void Start()
    {
        OnStart();
    }

    private void OnEnable()
    {
        // Register Listeners
    }

    private void OnDisable()
    {
        // Unregister Listeners
    }

    // Called after Awake is called
    protected virtual void OnAwake()
    {

    }

    // Called after Start is called
    protected virtual void OnStart()
    {

    }

    public virtual void Init(System.Action<GameObject> onCanceled)
    {
        if (onCanceled != null)
            OnCanceled = onCanceled;
    }

    public void Close()
    {
        OnCanceled?.Invoke(gameObject);
    }
}