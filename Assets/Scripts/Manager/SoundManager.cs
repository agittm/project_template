﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hellmade.Sound;

public class SoundManager : Manager
{
    [SerializeField] List<SoundClip> SFXClips;
    [SerializeField] List<SoundClip> BGMClips;
    [SerializeField] List<SoundClip> VOClips;

    private Dictionary<string, AudioClip> clipDict = new Dictionary<string, AudioClip>();
    private Dictionary<string, int> cachedClip = new Dictionary<string, int>();
    private List<ListenerData> listenerClips = new List<ListenerData>();

    protected override void OnAwake()
    {
        base.OnAwake();
        Init();
    }

    private void OnEnable()
    {
        EazySoundManager.OnFinishedPlaying += HandleOnAudioFinishPlaying;
    }

    private void OnDisable()
    {
        EazySoundManager.OnFinishedPlaying -= HandleOnAudioFinishPlaying;
    }

    private void Init()
    {
        transform.position = Camera.main.transform.position;

        for (int i = 0; i < SFXClips.Count; i++)
        {
            clipDict.Add(SFXClips[i].clipId, SFXClips[i].clip);
        }

        for (int i = 0; i < BGMClips.Count; i++)
        {
            clipDict.Add(BGMClips[i].clipId, BGMClips[i].clip);
        }

        for (int i = 0; i < VOClips.Count; i++)
        {
            clipDict.Add(VOClips[i].clipId, VOClips[i].clip);
        }

        PrepareAudio();

        Logger.Log("Initializing sound");
    }

    private void PrepareAudio()
    {
        for (int i = 0; i < BGMClips.Count; i++)
        {
            int id = EazySoundManager.PrepareMusic(BGMClips[i].clip, EazySoundManager.GlobalMusicVolume, true, true, 1f, 1f);
            cachedClip.Add(BGMClips[i].clipId, id);
        }

        for (int i = 0; i < SFXClips.Count; i++)
        {
            int id = EazySoundManager.PrepareSound(SFXClips[i].clip, EazySoundManager.GlobalSoundsVolume);
            cachedClip.Add(SFXClips[i].clipId, id);
        }

        for (int i = 0; i < VOClips.Count; i++)
        {
            int id = EazySoundManager.PrepareSound(VOClips[i].clip, EazySoundManager.GlobalSoundsVolume);
            cachedClip.Add(VOClips[i].clipId, id);
        }
    }

    public bool IsMuted()
    {
        return EazySoundManager.GlobalVolume == 0f;
    }

    public void SetMute(bool isMute)
    {
        EazySoundManager.GlobalVolume = isMute ? 0f : 1f;
    }

    public void ToggleMute()
    {
        SetMute(!IsMuted());
    }

    public void PlayBGM(string clipId)
    {
        if (!clipDict.ContainsKey(clipId))
        {
            Logger.LogErrorFormat("Clip not found : {0}", clipId);
            return;
        }

        if (cachedClip.ContainsKey(clipId))
        {
            if (EazySoundManager.GetMusicAudio(cachedClip[clipId]) != null)
            {
                Logger.Log("play from cached");
                EazySoundManager.GetMusicAudio(cachedClip[clipId]).Play();
            }
            else
            {
                // caching error, recall tbe function
                cachedClip.Remove(clipId);
                PlayBGM(clipId);
            }
        }
        else
        {
            Logger.Log("play new");
            int id = EazySoundManager.PlayMusic(clipDict[clipId], EazySoundManager.GlobalMusicVolume, true, true, 1f, 1f);
            cachedClip.Add(clipId, id);
        }
    }

    public void StopBGM(string clipId)
    {
        if (cachedClip.ContainsKey(clipId))
        {
            EazySoundManager.GetAudio(cachedClip[clipId]).Stop();
        }
    }

    public void PlaySFX(string clipId, Action<string> onFinished = null)
    {
        if (!clipDict.ContainsKey(clipId))
        {
            Logger.LogErrorFormat("Clip not found : {0}", clipId);
            return;
        }

        int id = EazySoundManager.PlaySound(clipDict[clipId], EazySoundManager.GlobalSoundsVolume);

        if (onFinished != null)
        {
            listenerClips.Add(new ListenerData(id, clipId, onFinished));
        }
    }

    public void PlayVO(string clipId, Action<string> onFinished = null)
    {
        if (!clipDict.ContainsKey(clipId))
        {
            Logger.LogErrorFormat("Clip not found : {0}", clipId);
            return;
        }

        int id = EazySoundManager.PlaySound(clipDict[clipId], EazySoundManager.GlobalSoundsVolume);

        if (onFinished != null)
        {
            listenerClips.Add(new ListenerData(id, clipId, onFinished));
        }
    }

    private void HandleOnAudioFinishPlaying(int audioId)
    {
        // has listener
        if (listenerClips.Exists(x => x.AudioID == audioId))
        {
            ListenerData listener = listenerClips.Find(x => x.AudioID == audioId);
            listener.Invoke();
            listenerClips.Remove(listener);
        }
    }

    public struct ListenerData
    {
        public int AudioID;
        public string ClipID;
        public Action<string> Listener;

        public ListenerData(int audioId, string clipId, Action<string> listener)
        {
            AudioID = audioId;
            ClipID = clipId;
            Listener = listener;
        }

        public void Invoke()
        {
            Listener?.Invoke(ClipID);
        }
    }

    #region Static Utility

    public static SoundManager Instance
    {
        get
        {
            return Get<SoundManager>();
        }
    }

    #endregion
}

[Serializable]
public class SoundClip
{
    public string clipId;
    public AudioClip clip;
}