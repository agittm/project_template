﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if USE_ANALYTICS_FA
using Firebase.Analytics;
#endif
#if USE_ANALYTICS_GA
using GameAnalyticsSDK;
#endif
#if USE_ANALYTICS_UA
using UnityEngine.Analytics;
#endif

public class AnalyticsManager : Manager
{
    [Tooltip("Add USE_ANALYTICS_GA in scripting symbol")]
    [SerializeField] bool useGameAnalytic;

    [Tooltip("Add USE_ANALYTICS_UA in scripting symbol")]
    [SerializeField] bool useUnityAnalytic;

    [Tooltip("Add USE_ANALYTICS_FA in scripting symbol")]
    [SerializeField] bool useFirebaseAnalytic;

    protected override void OnAwake()
    {
        base.OnAwake();
    }

    public void Initialize()
    {
#if USE_ANALYTICS_GA
        if (useGameAnalytic)
        {
            GameAnalytics.Initialize();
        }
#endif
    }

    public void RegisterEvent(string eventName, string eventParam, float eventValue)
    {
#if USE_ANALYTICS_FA
        if (useFirebaseAnalytic)
        {
            FirebaseAnalytics.LogEvent(eventName, eventParam, eventValue);
        }
#endif

#if USE_ANALYTICS_UA
        if (useUnityAnalytic)
        {
            AnalyticsEvent.Custom(
                eventName,
                new Dictionary<string, object> {
                    { eventParam, eventValue }
                }
            );
        }
#endif

#if USE_ANALYTICS_GA
        if (useGameAnalytic)
        {
            GameAnalytics.NewDesignEvent(eventName + ":" + eventParam, eventValue);
        }
#endif
    }

    public void RegisterEvent(string eventName, string eventParam, int eventValue)
    {
#if USE_ANALYTICS_FA
        if (useFirebaseAnalytic)
        {
            FirebaseAnalytics.LogEvent(eventName, eventParam, eventValue);
        }
#endif

#if USE_ANALYTICS_UA
        if (useUnityAnalytic)
        {
            AnalyticsEvent.Custom(
                eventName,
                new Dictionary<string, object> {
                    { eventParam, eventValue }
                }
            );
        }
#endif

#if USE_ANALYTICS_GA
        if (useGameAnalytic)
        {
            GameAnalytics.NewDesignEvent(eventName + ":" + eventParam, eventValue);
        }
#endif
    }

    public void RegisterEvent(string eventName, string eventParam, string eventValue)
    {
#if USE_ANALYTICS_FA
        if (useFirebaseAnalytic)
        {
            FirebaseAnalytics.LogEvent(eventName, eventParam, eventValue);
        }
#endif

#if USE_ANALYTICS_UA
        if (useUnityAnalytic)
        {
            AnalyticsEvent.Custom(
                eventName,
                new Dictionary<string, object> {
                    { eventParam, eventValue }
                }
            );
        }
#endif

#if USE_ANALYTICS_GA
        if (useGameAnalytic)
        {
            GameAnalytics.NewDesignEvent(eventName + ":" + eventParam + ":" + eventValue);
        }
#endif
    }

    #region Static Utility

    public static AnalyticsManager Instance
    {
        get
        {
            return Get<AnalyticsManager>();
        }
    }

    #endregion
}