﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController:MonoBehaviour
{
    public static void LoadScene(string sceneName, LoadSceneMode mode = LoadSceneMode.Single)
    {
        SceneManager.LoadScene(sceneName, mode);
    }

    public static AsyncOperation LoadSceneAsync(string sceneName, LoadSceneMode mode = LoadSceneMode.Additive)
    {
        return SceneManager.LoadSceneAsync(sceneName, mode);
    }

    public static AsyncOperation UnloadSceneAsync(string sceneName)
    {
        return SceneManager.UnloadSceneAsync(sceneName);
    }

    public static void SetActiveScene(string sceneName)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
    }

    public static string GetCurrentSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public static void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public static float GetSceneTime()
    {
        return Time.timeSinceLevelLoad;
    }
}