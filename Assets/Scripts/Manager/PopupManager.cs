﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : Manager
{
    private Action OnPopupClosed;
    private Action OnPopupConfirmed;

    [SerializeField] GameObject prefabPopupLoading;
    [SerializeField] GameObject prefabPopupInform;
    [SerializeField] GameObject prefabPopupConfirm;

    private GameObject popupLoading = null;
    private GameObject popupInform = null;
    private GameObject popupConfirm = null;

    protected override void OnAwake()
    {
        base.OnAwake();
    }

    public void ShowLoading(string title = "")
    {
        if (popupLoading == null)
        {
            popupLoading = Instantiate(prefabPopupLoading);
        }

        popupLoading.GetComponent<PopupModel>().InitLoading(title, () =>
        {
            Destroy(popupLoading);
        });
    }

    public void HideLoading()
    {
        if (popupLoading != null)
        {
            popupLoading.GetComponent<PopupModel>().Close();
        }
    }

    public void ShowInformation(string title, string content, string buttonCloseText, Action onClosed = null)
    {
        if (popupInform == null)
        {
            popupInform = Instantiate(prefabPopupInform);
        }

        if (onClosed != null)
            OnPopupClosed = onClosed;

        popupInform.GetComponent<PopupModel>().InitInformation(
            title,
            content,
            buttonCloseText,
            () => { Destroy(popupInform); onClosed?.Invoke(); });
    }

    public void ShowConfirmation(string title, string content, string buttonOKText, string buttonCloseText, bool isButtonReversed, Action onConfirmed = null, Action onClosed = null)
    {
        if (popupConfirm == null)
        {
            popupConfirm = Instantiate(prefabPopupConfirm);
        }

        if (onConfirmed != null)
            OnPopupConfirmed = onConfirmed;
        if (onClosed != null)
            OnPopupClosed = onClosed;

        popupConfirm.GetComponent<PopupModel>().InitConfirmation(
            title,
            content,
            buttonOKText,
            buttonCloseText,
            isButtonReversed,
            () => { Destroy(popupConfirm); onConfirmed?.Invoke(); },
            () => { Destroy(popupConfirm); onClosed?.Invoke(); });
    }

    #region Static Utility

    public static PopupManager Instance
    {
        get
        {
            return Get<PopupManager>();
        }
    }

    #endregion
}