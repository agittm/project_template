﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SceneTransition : Manager
{
    private Action OnFinishShowing;
    private Action OnFinishHiding;

    [Header("Setting")]
    [SerializeField] float minWaitTime;
    [SerializeField] string hideTriggerName = "Hide";
    [SerializeField] string showTriggerName = "Show";
    [SerializeField] GameObject transitionObject;

    private Animator animator;
    private AnimationEvent animationEvent;

    private bool isAnimationFinished;
    public bool IsAnimationFinished { get { return isAnimationFinished; } }

    protected override void OnAwake()
    {
        base.OnAwake();
        animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        animationEvent = GetComponent<AnimationEvent>();
        animationEvent.OnFinished.AddListener(HandleOnAnimationFinished);
    }

    private void OnDisable()
    {
        animationEvent.OnFinished.RemoveListener(HandleOnAnimationFinished);
    }

    public void StartTransition(string sceneToLoad, Action onFinish = null)
    {
        OnFinishHiding = onFinish;

        StartCoroutine(Transition(sceneToLoad));
    }

    private IEnumerator Transition(string sceneToLoad)
    {
        bool finishShowing = false;
        Show(() =>
        {
            finishShowing = true;
        });

        yield return new WaitUntil(() => finishShowing);
        
        AsyncOperation async = SceneController.LoadSceneAsync(sceneToLoad, UnityEngine.SceneManagement.LoadSceneMode.Additive);

        yield return new WaitUntil(() => async.isDone);

        AsyncOperation async2 = SceneController.UnloadSceneAsync(SceneController.GetCurrentSceneName());

        yield return new WaitUntil(() => async2.isDone);

        Hide();
    }

    public void Show(Action action = null)
    {
        OnFinishShowing = action;

        StartCoroutine(_Show());
    }

    private IEnumerator _Show()
    {
        isAnimationFinished = false;

        if (animator != null)
        {
            animator.SetTrigger(showTriggerName);
            yield return new WaitUntil(() => isAnimationFinished);
        }

        yield return new WaitForSecondsRealtime(minWaitTime);
        OnFinishShowing?.Invoke();
    }

    public void Hide(Action action = null)
    {
        OnFinishHiding = action;

        StartCoroutine(_Hide());
    }

    private IEnumerator _Hide()
    {
        isAnimationFinished = false;

        if (animator != null)
        {
            animator.SetTrigger(hideTriggerName);
            yield return new WaitUntil(() => isAnimationFinished);
        }

        yield return new WaitForSecondsRealtime(minWaitTime);

        OnFinishHiding?.Invoke();
        OnFinishHiding = null;
    }

    private void HandleOnAnimationFinished(string code)
    {
        if (code == showTriggerName)
        {
            isAnimationFinished = true;
        }
        else if(code == hideTriggerName)
        {
            isAnimationFinished = true;
        }
    }

    #region Static Utility

    public static SceneTransition Instance
    {
        get
        {
            return Get<SceneTransition>();
        }
    }

    #endregion
}
