﻿using UnityEngine;

public class CanvasCamera : MonoBehaviour
{
    [SerializeField] bool useMainCamera = true;
    [SerializeField] Camera targetCamera;
    [SerializeField] string layerName = "Default";
    [SerializeField] int layerOrder = 0;

    private void Awake()
    {
        if (useMainCamera)
            GetComponent<Canvas>().worldCamera = Camera.main;
        else if (targetCamera != null)
            GetComponent<Canvas>().worldCamera = targetCamera;

        GetComponent<Canvas>().sortingLayerName = layerName;
        GetComponent<Canvas>().sortingOrder = layerOrder;

        Canvas[] canvases = GetComponentsInChildren<Canvas>();
        if (canvases.Length > 0)
        {
            foreach (Canvas canvas in canvases)
            {
                canvas.sortingLayerName = layerName;
            }
        }
    }
}