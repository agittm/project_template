﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupModel : MonoBehaviour
{
    public delegate void PopupAction();
    private event PopupAction OnPopupLoadingClosed;
    private event PopupAction OnPopupInformClosed;
    private event PopupAction OnPopupConfirmClosed;
    private event PopupAction OnPopupConfirmOK;

    public enum PopupType { Loading, Info, Confirm }

    [Header("Setting")]
    [SerializeField] PopupType popupType;
    [SerializeField] bool confirmButtonReversed;

    [Header("Component")]
    [SerializeField] RectTransform content;
    [SerializeField] Text textTitle;
    [SerializeField] Text textContent;
    [SerializeField] Button buttonOK;
    [SerializeField] Text textButtonOK;
    [SerializeField] Button buttonClose;
    [SerializeField] Text textButtonClose;

    private bool isConfirmed;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (GetComponent<Animator>() == null)
        {
            LeanTween.scale(content, Vector2.zero, 0f);
            LeanTween.scale(content, Vector2.one, 0.5f).setEaseOutExpo();
        }
    }

    public void InitLoading(string title = "", PopupAction closeAction = null)
    {
        OnPopupLoadingClosed = closeAction;

        // Set Title
        textTitle.text = title;
    }

    public void InitInformation(string title, string content, string buttonCloseText = "Close", PopupAction onClose = null)
    {
        OnPopupInformClosed = onClose;

        if (buttonCloseText.Length <= 3)
        {
            buttonCloseText = "  " + buttonCloseText + "  ";
        }

        textTitle.text = title;
        textContent.text = content;
        textButtonClose.text = buttonCloseText;
    }

    public void InitConfirmation(string title, string content, string buttonOKText = "OK", string buttonCloseText = "Close", bool isButtonReversed = false, PopupAction onOK = null, PopupAction onClose = null)
    {
        OnPopupConfirmOK = onOK;
        OnPopupConfirmClosed = onClose;

        if (buttonOKText.Length <= 3)
        {
            buttonOKText = "  " + buttonOKText + "  ";
        }
        if (buttonCloseText.Length <= 3)
        {
            buttonCloseText = "  " + buttonCloseText + "  ";
        }

        confirmButtonReversed = isButtonReversed;
        textTitle.text = title;
        textContent.text = content;
        textButtonOK.text = buttonOKText;
        textButtonClose.text = buttonCloseText;

        if (confirmButtonReversed)
        {
            // Reverse the button
        }
    }

    public void Confirm()
    {
        isConfirmed = true;
        Close();
    }

    public void Close()
    {
        if (GetComponent<Animator>() != null)
        {
            GetComponent<Animator>().SetTrigger("Exit");
        }
        else
        {
            LeanTween.scale(content, Vector2.zero, 0.5f).setEaseOutExpo().setOnComplete(() =>
            {
                FinishAnimation();
            });
        }
    }

    public void FinishAnimation()
    {
        // only 1 invoke will be called
        OnPopupLoadingClosed?.Invoke();
        OnPopupInformClosed?.Invoke();

        if (isConfirmed)
        {
            OnPopupConfirmOK?.Invoke();
        }
        else
        {
            OnPopupConfirmClosed?.Invoke();
        }
    }
}