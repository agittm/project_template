﻿using UnityEngine;
using UnityEngine.Events;

public class ChangeSceneButton : BasicButton
{
    [Header("Change Scene Setting")]
    [SerializeField] SceneField targetScene;
    [SerializeField] bool useTransition;
    [SerializeField] UnityEvent onFinishTransition;

    public override void Click()
    {
        base.Click();

        if (useTransition)
        {
            SceneTransition.Instance.StartTransition(targetScene.SceneName);
        }
        else
        {
            SceneController.LoadScene(targetScene.SceneName);
        }
    }
}