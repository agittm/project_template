﻿using UnityEngine;
using UnityEngine.Events;

public class BasicButton : MonoBehaviour
{
    [SerializeField] protected float clickInterval = 0.5f;
    public UnityEvent OnClick;

    private bool clickable = true;

    public virtual void Click()
    {
        if (clickable)
        {
            clickable = false;
            PlaySFX();
            LeanTween.scale(gameObject.GetComponent<RectTransform>(), Vector2.one * 0.9f, clickInterval).setEasePunch().setOnComplete(
                () =>
                {
                    OnClick?.Invoke();
                    clickable = true;
                });
        }
    }

    public virtual void PlaySFX()
    {
        SoundManager.Instance.PlaySFX("button_basic");
    }
}