﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    public delegate void MovingAction(GameObject obj);
    public event MovingAction OnStartMoving;
    public event MovingAction OnMoving;
    public event MovingAction OnFinishMoving;

    [Header("Setting")]
    public float moveSpeed;
    [SerializeField] bool ignoreX;
    [SerializeField] bool ignoreY;

    [Header("Pre-defined target")]
    [SerializeField] Transform target;

    [Header("Info")]
    [SerializeField] protected Vector3 nextPos;
    [SerializeField] protected bool isMoving;

    private void Update()
    {
        if (isMoving)
        {
            if (nextPos != transform.localPosition)
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, nextPos, Time.deltaTime * moveSpeed);

                if (OnMoving != null)
                    OnMoving(gameObject);
            }
            else
            {
                FinishMoving();
            }
        }
    }

    public void Init(bool ignoreX = false, bool ignoreY = false)
    {
        this.ignoreX = ignoreX;
        this.ignoreY = ignoreY;
    }

    public void MoveTo(Vector3 targetPos, MovingAction onFinishMoving = null)
    {
        if (onFinishMoving != null)
            OnFinishMoving = onFinishMoving;

        nextPos = targetPos;
        StartMoving();
    }

    public void MoveTo(Transform target, MovingAction onFinishMoving = null)
    {
        MoveTo(target.position, onFinishMoving);
    }

    public void MoveToDeterminedObject(MovingAction onFinishMoving = null)
    {
        if (onFinishMoving != null)
            OnFinishMoving = onFinishMoving;

        nextPos = target.localPosition;
        StartMoving();
    }

    public virtual void StartMoving()
    {
        if (ignoreX)
            nextPos.x = transform.localPosition.x;
        if (ignoreY)
            nextPos.y = transform.localPosition.y;
        nextPos.z = 0;

        isMoving = true;

        if (OnStartMoving != null)
            OnStartMoving(gameObject);
    }

    public virtual void FinishMoving()
    {
        StopMoving();

        if (OnFinishMoving != null)
            OnFinishMoving(gameObject);
    }

    public void StopMoving()
    {
        isMoving = false;
    }
}